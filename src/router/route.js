import Vue from 'vue'
import VueRouter from 'vue-router'
import Zero from "../components/items/Zero";
import One from "../components/items/One";
import Two from "../components/items/Two";
import Three from "../components/items/Three";

Vue.use(VueRouter)

const router = new VueRouter({
    routes: [
        // dynamic segments start with a colon
        { path: '/', component: Zero },
        { path: '/1', component: One },
        { path: '/2', component: Two },
        { path: '/3', component: Three },

    ]
});

export default router;
